Here are the source code of the three core scripts.
1.Ansible_mrc:
	Used to create multiple instances on mrc
2.search_app and stream_app：
	Two docker container with build-up file. You can either git pull these file and run docker-compose up, or pull the uploaded
	corresponding docker images then run the flask app.

Here are instructions of using git
Always 'git checkout <branchname>' before you pull the code
Always 'git pull'
If you are adding new code:
	1. Using 'git add <newfile>' to add
	2. Using 'git commit -m' to describe individual added file
	3. git push
If you are upgrading code:
	1. Using git commit -a to identify and varify all changes
	2. git push