from flask import Flask
import os
from tweepy import Client

app= Flask(__name__)


@app.route('/')
def search_tweets():
	bearer_token = 'AAAAAAAAAAAAAAAAAAAAAFGrbQEAAAAAJSiN63IaZ%2BpUwqLK3uZudv7k6FM%3DCdGEHagQtisfbrJkwaQpTzm88NZw7zjzjFAtphqjfLpJeAoOek'

	if not bearer_token:
		return "Bearer token not specified"
	client = Client(bearer_token)
	query = '-is:retweet (melbourne OR #Melbourne OR #MELBOURNE) lang:en (rent OR "house rent" OR #houserent) -see -mind -head -book -car' 
	max_results = 100 #Basic account has to be between 10 to 100
	counter = 0
	#Getting results
	results = client.search_recent_tweets(query,max_results=max_results)
	if results.errors:
		raise RuntimeError(results.errors)
	if results.data:
		for tweet in results.data:
			print(tweet.__repr__())
			counter+=1

	while True:
		try:
			next=results.meta["next_token"]
		except:
			return results.meta

		results=client.search_recent_tweets(query,max_results=max_results,next_token=results.meta[next])
		if results.errors:
			raise RuntimeError(results.errors)
		if results.data:
			for tweet in results.data:
				print(tweet.__repr_())
				counter+=1

if __name__ == "__main__":
	app.run(host='0.0.0.0',debug=True)
