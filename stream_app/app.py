from flask import Flask
import os
from tweepy import StreamingClient, StreamRule, Tweet

app= Flask(__name__)

class stream_tweets(StreamingClient):

    def on_tweet(self,tweet:Tweet):
        print(tweet.__repr__())

    def on_request_error(self, status_code):
        print(status_code)

    def on_connection_error(self):
        self.disconnect()


@app.route('/')
def streaming():
    bearer_token = 'AAAAAAAAAAAAAAAAAAAAAFGrbQEAAAAAJSiN63IaZ%2BpUwqLK3uZudv7k6FM%3DCdGEHagQtisfbrJkwaQpTzm88NZw7zjzjFAtphqjfLpJeAoOek'
    client = stream_tweets(bearer_token)
    rules = [StreamRule(value='-is:retweet (melbourne OR #Melbourne OR #MELBOURNE) lang:en (rent OR "house rent" OR #houserent) -see -mind -head -book -car',tag="Melbourne houserent")]
    resp = client.get_rules()
    
    #Deleting existing rules
    if resp and resp.data:
        rule_ids = []
        for rule in resp.data:
            rule_ids.append(rule.id)

        client.delete_rules(rule_ids)

    # Validate the rule
    resp = client.add_rules(rules, dry_run=True)
    if resp.errors:
        raise RuntimeError(resp.errors)

    # Add the rule
    resp = client.add_rules(rules)
    if resp.errors:
        raise RuntimeError(resp.errors)

    # https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream
    try:
        client.filter()
    except KeyboardInterrupt:
        client.disconnect()

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)